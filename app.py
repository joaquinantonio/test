# File: app.py
def add(x, y):
    return x + y

# File: test_app.py
from app import add

def test_add():
    assert add(2, 3) == 5

